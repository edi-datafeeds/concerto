EDI_AGNCY_20170623
TableName	Actflag	Created	Changed	AgncyID	RegistrarName	Add1	Add2	Add3	Add4	Add5	Add6	City	CntryCD	Website	Contact1	Tel1	Fax1	Email1	Contact2	Tel2	Fax2	Email2	Depository	State
AGNCY	I	2017/06/23	2017/06/23	36610	Greenko Energy Holdings	c/o Estera Management (Mauritius) Limited	La Chausee Street	11th Floor, M�dine Mews	Port Louis	Mauritius			MU										F	
AGNCY	I	2017/06/23	2017/06/23	36611	XL Group Ltd																		F	
AGNCY	I	2017/06/23	2017/06/23	36612	Broadridge�Corporate�Issues�Solutions,�Inc.								US										F	
AGNCY	I	2017/06/23	2017/06/23	36613	Huishang Bank..	Block A, Tianhui Tower, No.79,	Anqing Road, Hefei, Anhui								JIA Ruiyue, ZHANG Lu	86551-65970432	86551-65195319						F	
AGNCY	I	2017/06/23	2017/06/23	36614	China Zheshang Bank..,	No.A-1, Finance Avenue,	Xicheng District, Beijing																F	
AGNCY	I	2017/06/23	2017/06/23	36615	Road King Infrastructure Limited	Suite 501	5th Floor, Tower 6	The Gateway	9 Canton Road	Tsimshatsui, Kowloon			HK										F	
AGNCY	I	2017/06/23	2017/06/23	36616	China Development Bank...,,	East side, 11F, No.18,	Fu Xing Men Inner Street,	Xicheng District, Beijing							WANG Xiatian	8610-88303629	8610-88303364						F	
AGNCY	I	2017/06/23	2017/06/23	36617	Stoneway Energy International LP,																		F	
AGNCY	I	2017/06/23	2017/06/23	36618	Stoneway Energy LP	New Brunswick, Canada																	F	
AGNCY	I	2017/06/23	2017/06/23	36619	The Land SPV,																		F	
AGNCY	I	2017/06/23	2017/06/23	36620	Setterwalls Advokatbry� AB	Sturegatan 10	P.O. Box 1050	SE-101 39 Stockholm															F	
AGNCY	I	2017/06/23	2017/06/23	36621	Huaxi Securities,..	8F, Sinochem Tower,	No.A2, Fu Xing Men Wai Street,	Xicheng District,	Beijing				CN										F	
AGNCY	I	2017/06/23	2017/06/23	36622	Beijing Zhongguancun Sci-tech Financing Guaranty Co.Ltd	30f, Block A, Building No.1,	No.12, Zhongguancun South Street,	Haidian District,	Beijing				CN										F	
AGNCY	I	2017/06/23	2017/06/23	36623	RELX NV	Radarweg 29,	1043 NX,	Amsterdam					NL										F	
AGNCY	I	2017/06/23	2017/06/23	36624	Baker McKenzie LLP	100 New Bridge Street	London EC4V 6JA						GB										F	
AGNCY	I	2017/06/23	2017/06/23	36625	Jain Irrigation Systems Limited	Jain Plastic Park, N.H. No. 6, Bambhori	Jalgaon 425001	India					IN										F	
AGNCY	I	2017/06/23	2017/06/23	36626	Cristiano di thiene S.P.A.																		F	
AGNCY	I	2017/06/23	2017/06/23	36627	Foresight Energy LP																		F	
AGNCY	I	2017/06/23	2017/06/23	36628	Chalco HongKong Ltd.	Room 4501	Far East Finance Centre	No.16 Harcourt Road	Admiralty, Hong Kong				HK										F	
AGNCY	I	2017/06/23	2017/06/23	36629	Creative Touch Interiors, Inc																		F	
AGNCY	I	2017/06/23	2017/06/23	36630	HD Supply Construction Supply Group, Inc.																		F	
AGNCY	I	2017/06/23	2017/06/23	36631	HD Supply FM Services, LLC																		F	
AGNCY	I	2017/06/23	2017/06/23	36632	Codere, S.A.																		F	
AGNCY	I	2017/06/23	2017/06/23	36633	Bingos Platenses S.A.																		F	
AGNCY	I	2017/06/23	2017/06/23	36634	GLAS Americas LLC.																		F	
AGNCY	I	2017/06/23	2017/06/23	36635	The Bank Of New York Mellon..,,,	500 Ross Street,	12th Floor,	Pittsburgh, PA 15262.															F	
AGNCY	I	2017/06/23	2017/06/23	36636	Compass Group plc																		F	
AGNCY	I	2017/06/23	2017/06/23	36637	Perpetual Corporate Trust Limited,	Level 18,	123 Pitt Street,	Sydney,	NSW 2000 Australia.			Sydney	AU										F	
AGNCY	I	2017/06/23	2017/06/23	36638	Ra�zen Combust�veis S.A.	Rua Victor Civita, No. 77, block 1,	Condom�nio Rio Office Park (ROP),	Jacar�pagua, Zip Code 22775-044,	Rio de Janeiro, RJ				BR										F	
AGNCY	I	2017/06/23	2017/06/23	36639	Ra�zen Energia S.A.	Avenida Juscelino Kubitschek, No. 1,327,	5th floor, room 01, Vila Nova Concei��o,	Zip Code 04543-011, S�o Paulo, SP					BR										F	
AGNCY	I	2017/06/23	2017/06/23	36640	Thomas Cook Finance plc																		F	
AGNCY	I	2017/06/23	2017/06/23	36641	Thomas Cook Group Treasury Limited																		F	
AGNCY	I	2017/06/23	2017/06/23	36642	Thomas Cook UK Limited																		F	
EDI_ENDOFFILE
